package ro.usv.rf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Florin
 */
public class Distances {
	private List<Double> cebisevDistances;
	private List<Double> mahalanovisDistances;
	private List<Double> cityBlockDistances;
	private List<Double> euclidianDistances;
	
	private double[] firstPattern;
	private double[][] learningSet;
	private int numberOfPatterns;

	public Distances(double[] firstPattern, double[][] learningSet, int numberOfPatterns) {
		this.firstPattern = firstPattern;
		this.learningSet = learningSet;
		this.numberOfPatterns = numberOfPatterns;
		calculateDistances();
	}

	private void calculateDistances() {
		cebisevDistances = new ArrayList<>();
		mahalanovisDistances = new ArrayList<Double>();
		cityBlockDistances = new ArrayList<Double>();
		euclidianDistances = new ArrayList<Double>();
		
		int rowIndex = 1;
		for (double[] pattern : learningSet) {
			if(pattern.equals(firstPattern)) {
				continue;
			}
			
			List<Double> cebisevDistances = new ArrayList<Double>();
			double mahalanovisDistances = 0.0;
			double cityBlockDistances = 0.0;
			double euclidianDistances = 0.0;
			
			for(int colIndex = 0; colIndex < firstPattern.length; colIndex++) {
				cebisevDistances.add(Math.abs(firstPattern[colIndex]) - Math.abs(learningSet[rowIndex][colIndex]));
				mahalanovisDistances += Math.pow(firstPattern[colIndex] - learningSet[rowIndex][colIndex], numberOfPatterns);
				cityBlockDistances += Math.abs(firstPattern[colIndex] - learningSet[rowIndex][colIndex]);
			}
			
			this.cebisevDistances.add(Collections.max(cebisevDistances));
			this.mahalanovisDistances.add(Math.pow(mahalanovisDistances, 1.0 / numberOfPatterns));
			this.cityBlockDistances.add(cityBlockDistances);
			
			rowIndex++;
		}
	}
	
	public List<Double> getMahalanovisDistances() {
		return mahalanovisDistances;
	}
	
	public List<Double> getCebisevDistances() {
		return cebisevDistances;
	}
	
	public List<Double> getCityBlockDistances() {
		return cityBlockDistances;
	}
	
	public void printCebisevDistances() {
		System.out.println("CEBISEV DISTANCES");
		for (double d : getCebisevDistances()) {
			System.out.print(d + "\t");
		}
		
		System.out.println("\n");
	}
	
	public void printMahalanovisDistances() {
		System.out.println("MAHALANOVIS DISTANCES");
		for (double d : getMahalanovisDistances()) {
			System.out.printf("%.2f\t", d);
		}
		
		System.out.println("\n");
	}
	
	public void printCityBlockDistances() {
		System.out.println("CITYBLOCK DISTANCES");
		for (double d : getCityBlockDistances()) {
			System.out.printf("%.2f\t", d);
		}
		
		System.out.println("\n");
	}
	
}
