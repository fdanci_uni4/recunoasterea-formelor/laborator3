package ro.usv.rf;

public class MainClass {


	public static void main(String[] args) {
		double[][] learningSet;
		try {
			learningSet = FileUtils.readLearningSetFromFile("in.txt");
			int numberOfPatterns = learningSet.length;
			
			Distances distances = new Distances(learningSet[0], learningSet, numberOfPatterns);
			distances.printCebisevDistances();
			distances.printMahalanovisDistances();
			distances.printCityBlockDistances();
			
		} catch (USVInputFileCustomException e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("\nFinished learning set operations");
		}
	}

}
